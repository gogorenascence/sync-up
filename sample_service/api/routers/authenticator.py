import os
from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from api.routers.models import UserIn, UserOut
from db import UserQueries
from datetime import timedelta


class MyAuthenticator(Authenticator):
    async def get_account_data(
        self,
        email: str,
        accounts: UserQueries,
    ):
        return accounts.get_user(email)

    def get_account_getter(
        self,
        accounts: UserQueries = Depends(),
    ):
        return accounts

    def get_hashed_password(self, account: UserIn):
        return account.hashed_password

    def get_account_data_for_cookie(self, account: UserIn):
        return account.email, UserOut(**account.dict())


one_week = timedelta(days=7)

authenticator = MyAuthenticator(
    os.environ["SIGNING_KEY"],
    exp=one_week,
)
