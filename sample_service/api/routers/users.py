from fastapi import APIRouter, Depends, Response, Request, HTTPException
from api.routers.authenticator import authenticator
from api.routers.models import (
    UserIn,
    UserOut,
    UsersOut,
    UsersForm,
    UsersToken,
    HttpError,
)
from db import UserQueries
from builtins import Exception


router = APIRouter()


@router.get("/api/users", response_model=UsersOut)
def users_list(
    queries: UserQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return {
        "users": queries.get_all_users(),
    }


@router.post("/api/users/", response_model=UsersToken | HttpError)
async def create_user(
    info: UserIn,
    request: Request,
    response: Response,
    repo: UserQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    user = repo.create_user(info, hashed_password)
    form = UsersForm(
        username=info.email,
        password=info.password,
    )
    token = await authenticator.login(response, request, form, repo)
    return UsersToken(user=user, **token.dict())


@router.put("/api/users/{user_id}", response_model=UserOut)
def update_user(
    id: int,
    user_in: UserIn,
    response: Response,
    queries: UserQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        hashed_password = authenticator.hash_password(user_in.password)
        record = queries.update_user(id, user_in, hashed_password)
        return record
    except Exception:
        raise HTTPException(status_code=400, detail="Invalid input")


@router.delete("/api/users/{user_id}", response_model=bool)
def delete_user(
    user_id: int,
    queries: UserQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    queries.delete_user(user_id)
    return True


@router.get("/api/users/{email}", response_model=UserOut)
def get_user(
    email: str,
    response: Response,
    queries: UserQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        record = queries.get_user(email)
    except ValueError:
        raise HTTPException(status_code=400, detail="Invalid input")
    return record


@router.get("/api/user/current")
async def get_current_user_data(
    account_data=Depends(authenticator.try_get_current_account_data),
):
    return account_data


@router.get("/token")
async def get_token(
    request: Request,
    account=Depends(authenticator.try_get_current_account_data),
):
    if authenticator.cookie_name in request.cookies:
        print(authenticator.cookie_name)
        token_data = {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }
        return token_data
