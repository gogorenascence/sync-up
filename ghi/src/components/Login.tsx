import * as React from "react";
import { ChangeEvent } from "react";
import { ReactElement, useState, useEffect } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { Link, useNavigate } from "react-router-dom";
type props = {
  showLogin: boolean;
  setShowLogin: React.Dispatch<React.SetStateAction<boolean>>;
};
const Login = ({ showLogin, setShowLogin }: props): ReactElement => {
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const navigate = useNavigate();
  const { login, token } = useToken();
  const handleUsernameChange = (event: ChangeEvent<HTMLInputElement>): void => {
    const element = event.currentTarget as HTMLInputElement;
    setUsername(element.value);
  };
  const handlePasswordChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setPassword(event.currentTarget.value);
    // this is a good way
  };
  const handleSubmit = async (
    e: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    e.preventDefault();

    login(username, password);
    // navigate("/home");
    setUsername("");
    setPassword("");
  };

  useEffect(() => {
    if (token !== null) {
      navigate("/home");
    }
    // eslint-disable-next-line
  }, [token]);

  const closeModal = () => {
    setShowLogin(false);
  };

  return (
    <div>
      {showLogin && (
        <div className="fixed inset-0 flex items-center justify-center z-50">
          <div className="flex flex-col max-w-lg p-6 rounded-md  md:min-w-[500px] md:min-h-[500px]  bg-gray-900 text-gray-100">
            <div className="mb-8 text-center">
              <h1 className="my-3 text-4xl font-bold">Sign in</h1>
              <p className="text-sm text-gray-400">
                Sign in to access your account
              </p>
            </div>
            <form
              onSubmit={handleSubmit}
              className="space-y-12 ng-untouched ng-pristine ng-valid md:text-center"
            >
              <div className="space-y-4">
                <div>
                  <label
                    htmlFor="email"
                    className="block mb-2 text-sm md:text-2xl"
                  >
                    Email address
                  </label>
                  <input
                    className="text-gray-950"
                    onChange={handleUsernameChange}
                    type="email"
                    placeholder="helloworld@email.com"
                    value={username === null ? "" : username}
                  />
                </div>
                <div>
                  <div className="mb-2">
                    <label htmlFor="password" className="text-sm md:text-2xl">
                      Password
                    </label>
                  </div>
                  <input
                    className="text-gray-950"
                    onChange={handlePasswordChange}
                    placeholder="*********"
                    type="password"
                    value={password === null ? "" : password}
                  />
                </div>
              </div>
              <div className="space-y-2">
                <div>
                  <button
                    type="submit"
                    className="w-full px-8 py-3 font-semibold rounded-md bg-violet-400 text-gray-900"
                  >
                    Sign in
                  </button>
                </div>
                <p className="px-6 text-sm text-center text-gray-400">
                  Don't have an account yet?{" "}
                  <Link
                    rel="noopener noreferrer"
                    to="/signup"
                    className="hover:underline text-violet-400"
                  >
                    Sign up
                  </Link>
                  .
                </p>
              </div>
            </form>
            <button
              onClick={closeModal}
              className="self-center px-8 py-3 mt-4 font-semibold rounded-md bg-violet-400 text-gray-900"
            >
              Close
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default Login;
