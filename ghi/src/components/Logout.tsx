import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";
const Logout = () => {
  const { logout } = useToken();
  const navigate = useNavigate();
  const clickLogout = () => {
    logout();
    navigate("/");
  };
  return (
    <div>
      <button
        type="button"
        onClick={clickLogout}
        className="text-white bg-gradient-to-r from-red-400 via-red-500 to-red-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-red-300 dark:focus:ring-red-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
      >
        LogOut
      </button>
    </div>
  );
};

export default Logout;
