import { createContext, useEffect, useState, ReactNode } from "react";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { UserData } from "../types";
import { useContext } from "react";
import { UpdateContext } from "./UpdateContext";

export const UserContext = createContext<UserData | null>({
  id: 0,
  name: "",
  email: "",
  timezone: "",
  password: "",
  picture: "",
});

const useFetchData = (): UserData | null => {
  const { token } = useAuthContext();
  const [data, setData] = useState<UserData | null>(null);
  const updateC = useContext(UpdateContext);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(
          `${import.meta.env.VITE_BACKEND_BASE_URL}/api/user/current`,
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        );
        const jsonData: UserData = await response.json();
        setData(jsonData);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
  }, [token, updateC.update]);
  return data;
};

type Props = {
  children?: ReactNode;
};

export const UserContextProvider = ({ children }: Props) => {
  const fetchedData = useFetchData();
  return (
    <UserContext.Provider value={fetchedData}>{children}</UserContext.Provider>
  );
};
