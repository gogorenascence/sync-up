import React from "react";
import { useContext, MouseEvent } from "react";
import { GIDContext } from "../../assets/context/GIDContext";
import { GMContext } from "../../assets/context/GMContext";
type props = {
  showGroupList: boolean;
  setShowGroupList: React.Dispatch<React.SetStateAction<boolean>>;
};
const GroupDropDown = ({ showGroupList, setShowGroupList }: props) => {
  const { setGroupId } = useContext(GIDContext)!;
  const GMC = useContext(GMContext);
  const groupData = GMC?.groupData;

  const handleClickGroupID = (e: MouseEvent<HTMLButtonElement>) => {
    const value = Number(e.currentTarget.value);
    setGroupId!(value);
    setShowGroupList(false);
  };
  const closeModal = () => {
    setShowGroupList(false);
  };

  return (
    <>
      {showGroupList && (
        <div>
          <div className="fixed inset-0 flex items-center justify-center z-50">
            <div className="flex flex-col max-w-lg p-6 rounded-md  md:min-w-[500px] md:min-h-[500px]  bg-gray-900 text-gray-100">
              {groupData && (
                <ul className="py-2">
                  {groupData?.map((group) => (
                    <li key={group.id}>
                      <button
                        onClick={handleClickGroupID}
                        value={group.id}
                        className="block py-2  hover:bg-blue-500 text-white px-20 m-auto text-xl"
                      >
                        {group.name}
                      </button>
                    </li>
                  ))}
                </ul>
              )}
              <button
                onClick={closeModal}
                className="self-center px-8 py-3 mt-4 font-semibold rounded-md bg-violet-400 text-gray-900"
              >
                Close
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default GroupDropDown;
