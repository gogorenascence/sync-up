import { rickRoll } from "../../assets/memes/meme";

const FAqustion = () => {
  return (
    <section id="FAQ" className="bg-sky-800 text-neutral-200">
      <div className="container flex flex-col justify-center px-4 py-8 mx-auto md:p-8">
        <h2 className="text-2xl font-semibold md:text-4xl">
          Frequently Asked Questions
        </h2>
        <p className="mt-4 mb-8 text-xl md:text-2xl">
          I know you must have a lot of question to ask us, no problem, here are
          some of our FAQ!
        </p>
        <div className="space-y-4">
          <details className="w-full border rounded-lg">
            <summary className="px-4 text-xl py-6 focus:outline-none focus-visible:ring-violet-400">
              Why should we hire you as our software engineer?
            </summary>
            <p className="px-4 py-6 pt-0 ml-4 -mt-4 ">
              We worked very hard on this project, and we think we deserve a
              hire! We conquered many different problem in this app, and we
              successfully used different technology to build the website
            </p>
          </details>
          <details className="w-full border rounded-lg">
            <summary className="px-4 text-xl py-6 focus:outline-none focus-visible:ring-violet-400">
              Why is the best pasta/ noodle thin? steak fries is the best!
            </summary>
            <p className="px-4 py-6 pt-0 ml-4 -mt-4 ">
              The think Noodle generally give us more chance to have the sauce
              and produce a better result because of that! Also, steak fires are
              equally good as onion ring.
            </p>
          </details>
          <details className="w-full border rounded-lg">
            <summary
              className="px-4 text-xl py-6 focus:outline-none focus-visible:ring-violet-400"
              onClick={rickRoll}
            >
              Who is the question being asked? Who Am I? What is the meaning of
              life?
            </summary>
            <p className="px-4 py-6 pt-0 ml-4 -mt-4 ">
              Have you tried to party with our app? Its great, Lorem Loren Lore,
              yes you guessed it right, you gonna get rick rolled.
            </p>
          </details>
        </div>
      </div>
    </section>
  );
};

export default FAqustion;
