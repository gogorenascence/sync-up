import { easyStreet } from "../../assets/memes/meme";
export const StepInfo = () => {
  return (
    <article className="bg-sky-800 flex flex-row justify-between">
      <div className="pt-6 md:pt-16">
        <h1 className="text-2xl text-neutral-200 text-center md:text-6xl">
          3 easy steps to success 🏆
        </h1>
        <ul className="pt-4 flex flex-col justify-evenly text-neutral-200 md:flex-row md:pt-16 md:text-xl">
          <li className="px-6 py-4 flex flex-row md:flex-col md:px-4 ">
            <div className="flex items-center justify-center w-8 h-8 text-black bg-yellow-400 rounded-full shrink-0 md:m-auto">
              1
            </div>
            <div className="pl-2">
              Create Your online personal account to active and change your
              real-time availability.
            </div>
          </li>
          <li className="px-6 py-4 flex flex-row md:flex-col md:px-4">
            <div className="flex items-center justify-center w-8 h-8 text-black bg-yellow-400 rounded-full shrink-0 md:m-auto">
              2
            </div>
            <div className="pl-2">
              Create your own group, add more admin, have a new meeting, all
              with in ONE click.
            </div>
          </li>
          <li className="px-6 py-4 flex flex-row md:flex-col md:px-4">
            <div
              onClick={easyStreet}
              className="flex items-center justify-center w-8 h-8 text-black bg-yellow-400 rounded-full shrink-0 md:m-auto"
            >
              3
            </div>
            <div className="pl-2">
              Max out your efficiency by linking your Page with your email and
              receive email for each meeting.
            </div>
          </li>
        </ul>
      </div>
    </article>
  );
};
