import { Link } from "react-router-dom";
import { GMContext } from "../../assets/context/GMContext";
import { MIDContext } from "../../assets/context/MIDContext";
import { useContext, useState, useEffect } from "react";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { UserData } from "../../assets/types";
import NavMeeting from "./NavMeeting";
import AddUserToMeeting from "./AddToMeeting";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../../assets/context/UserContext";
const ViewMeeting = () => {
  const navigate = useNavigate();
  const GMData = useContext(GMContext);
  const meetingData = GMData?.meetingData;
  const GroupData = GMData?.groupData;
  const meetingIstuff = useContext(MIDContext);
  const meetingId = meetingIstuff.meetingId;
  const setMeetingId = meetingIstuff.setMeetingId;
  const currentMeetingFilter = meetingData?.find(
    (meeting) => meeting.id === meetingId
  );
  const userData = useContext(UserContext);
  const userID = userData?.id;
  const currentGroupId = currentMeetingFilter?.group_id;
  const currentGroup = GroupData?.find((group) => group.id === currentGroupId);
  const currentGroupName = currentGroup?.name;
  const { token } = useAuthContext();
  const [admin, setAdmin] = useState(false);

  const fetchAdmin = async () => {
    if (token !== null && userID !== null && currentGroupId !== null) {
      try {
        const response = await fetch(
          `${
            import.meta.env.VITE_BACKEND_BASE_URL
          }/api/update/group?group_id=${currentGroupId}&user_id=${userID}`,
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        );
        const jsonData = await response.json();
        setAdmin(jsonData.admin_status);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    }
  };
  useEffect(() => {
    fetchAdmin();
    // eslint-disable-next-line
  }, [currentGroupId]);

  const fetchMeetingData = GMData!.fetchMeetingData;
  const [addUserToMeetingM, setAddUserToMeetingM] = useState(false);
  const [userInMeeting, setUserInMeeting] = useState<UserData[] | null>(null);
  const fetchAllUserInMeeting = async () => {
    if (token !== null && meetingId !== null) {
      try {
        const response = await fetch(
          `${
            import.meta.env.VITE_BACKEND_BASE_URL
          }/api/meeting/${meetingId}/user`,
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        );
        const jsonData = await response.json();
        setUserInMeeting(jsonData);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    }
  };
  useEffect(() => {
    fetchAllUserInMeeting();
    // eslint-disable-next-line
  }, [meetingId, token]);
  const DeleteUserInMeeting = async () => {
    if (token !== null && meetingId !== null && userID !== null) {
      const data = [userID];
      const fetchConfig = {
        method: "delete",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      };
      try {
        await fetch(
          `${
            import.meta.env.VITE_BACKEND_BASE_URL
          }/api/user/meeting/delete?meeting_id=${meetingId}`,
          fetchConfig
        );
      } catch {
        console.log(event);
      } finally {
        fetchAllUserInMeeting();
        fetchMeetingData();
        navigate("/meeting");
      }
    }
  };
  const deleteMeeting = async () => {
    if (token !== null && meetingId !== null) {
      const fetchConfig = {
        method: "delete",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      };
      try {
        await fetch(
          `${
            import.meta.env.VITE_BACKEND_BASE_URL
          }/api/meeting/delete/${meetingId}`,
          fetchConfig
        );
      } catch {
        console.log(Error);
      } finally {
        setMeetingId(null);
        fetchMeetingData();
        navigate("/meeting");
      }
    }
  };
  return (
    <>
      <NavMeeting />
      {!meetingId && <div>Select a meeting to view details</div>}
      <AddUserToMeeting
        addUserToMeetingM={addUserToMeetingM}
        setAddUserToMeetingM={setAddUserToMeetingM}
        fetchAllUserInMeeting={fetchAllUserInMeeting}
      />
      {meetingId && (
        <div className="py-10">
          <div className="flex flex-col md:flex-row justify-evenly py-auto">
            <Link className="text-3xl" to="/meeting/update">
              Update Meeting
            </Link>
            <h1 className="text-4xl text-center mb-5">Meeting Details</h1>
            {admin && (
              <div>
                <button onClick={deleteMeeting} className="text-3xl">
                  Delete Meeting
                </button>
              </div>
            )}
          </div>
          <div className="flex flex-col md:flex-row justify-evenly">
            <div>
              <div className="max-w-md mx-auto bg-gray-200 p-6 rounded-lg shadow md:min-w-[400px]">
                <div className="mb-4">
                  <h2 className="text-xl font-bold mb-2">Meeting Name:</h2>
                  <p>{currentMeetingFilter?.name}</p>
                </div>
                <div className="mb-4">
                  <h2 className="text-xl font-bold mb-2">
                    Meeting Description:
                  </h2>
                  <p>{currentMeetingFilter?.descriptions}</p>
                </div>
                <div className="mb-4">
                  <h2 className="text-xl font-bold mb-2">Meeting Link:</h2>
                  <p>{currentMeetingFilter?.link}</p>
                </div>
                <div className="mb-4">
                  <h2 className="text-xl font-bold mb-2">Meeting Time:</h2>
                  <p>{currentMeetingFilter?.time}</p>
                </div>
                <div className="mb-4">
                  <h2 className="text-xl font-bold mb-2">Group Name:</h2>
                  <p>{currentGroupName}</p>
                </div>
              </div>
            </div>
            <div>
              <div className="max-w-md mx-auto bg-gray-200 p-6 rounded-lg shadow md:min-w-[400px]">
                <div className="text-center pb-3">
                  <button
                    onClick={() => setAddUserToMeetingM(true)}
                    className="text-white bg-gray-800 focus:outline-none focus:ring-4 focus:ring-gray-300 font-medium rounded-full text-sm px-5 py-2.5 mr-2 mb-2"
                  >
                    Add a user
                  </button>
                  <button
                    onClick={DeleteUserInMeeting}
                    className="text-white bg-gray-800 focus:outline-none focus:ring-4 focus:ring-gray-300 font-medium rounded-full text-sm px-5 py-2.5 mr-2 mb-2"
                  >
                    Leave Meeting
                  </button>
                </div>
                <table className="w-full text-sm text-left text-gray-500">
                  <thead className="text-xs text-gray-700 uppercase bg-gray-50">
                    <tr>
                      <th scope="col" className="px-6 py-3 text-xl">
                        User Name
                      </th>
                      <th scope="col" className="px-6 py-3 text-xl">
                        User Email
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {userInMeeting &&
                      userInMeeting.map((user) => (
                        <tr
                          key={user.id}
                          className="bg-white border-b border-gray-700"
                        >
                          <td className="px-6 py-4 text-xl">{user.name}</td>
                          <td className="px-6 py-4 text-xl">{user.email}</td>
                        </tr>
                      ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};
export default ViewMeeting;
